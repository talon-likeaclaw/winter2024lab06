public class GameManager {
	// fields
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	// constructor
	public GameManager() {
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.playerCard = this.drawPile.drawTopCard(1);
		this.centerCard = this.drawPile.drawTopCard(1);
	}
	
	// toString()
	public String toString() {
		return "Center card: " + this.centerCard + "\n" + "Player card: " + this.playerCard;
	}
	
	// methods
	public void dealCards() {
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard(1);
		this.drawPile.shuffle();
		this.playerCard = this.drawPile.drawTopCard(1);
	}
	
	public int getNumberOfCards() {
		return drawPile.getNumOfCards();
	}
	
	public int calculatePoints() {
		if (this.centerCard.getValue().equals(this.playerCard.getValue())) {
			return 4;
		} else if (this.centerCard.getSuit().equals(this.playerCard.getSuit())) {
			return 2;
		} else {
			return -1;
		}
	}
	
	
}