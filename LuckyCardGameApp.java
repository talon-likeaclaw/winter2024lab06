public class LuckyCardGameApp {
	
	public static void main(String[] args) {
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int roundNum = 1;
		// Play the game
		System.out.println("Hello! Welcome to my lucky card game!");
		while (totalPoints < 5 && manager.getNumberOfCards() != 0) {
			System.out.println("Round: " + roundNum);
			System.out.println(manager);
			totalPoints += manager.calculatePoints();
			manager.dealCards();
			roundNum++;
			System.out.println("Your points after round " + (roundNum - 1) + ": " + totalPoints);
		}
		if (totalPoints >= 5) {
			System.out.println("Congrats!! You won!!");
		} else {
			System.out.println("Sorry, you lost! Try again!");
		}
	}
	
}