import java.util.Random;

public class Deck {
	
	// fields
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	// constructor
	public Deck() {
		this.numberOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[52];
		
		String[] suits = new String[] {"Heart","Diamond","Spade","Club"};
		String[] values = new String[] {"Ace","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King"};
		int index = 0;
		for (int s = 0; s < suits.length; s++) {
			for(int v = 0; v < values.length; v++) {
				this.cards[index] = new Card(suits[s], values[v]);
				index++;
			}
		}
	}
	
	// getters
	public int getNumOfCards() {
		return this.numberOfCards;
	}
	
	// toString()
	public String toString() {
		String deckString = "";
		for (int i = 0; i < numberOfCards; i++) {
			deckString += cards[i] + " \n";
		}
		return deckString;
	}
	
	// methods
	public int length() {
		return this.numberOfCards;
	}
	
	public Card drawTopCard(int numRemove) {
		this.numberOfCards = this.numberOfCards - numRemove;
		return this.cards[this.numberOfCards];
	}
	
	public void shuffle() {
		for (int i = 0; i < numberOfCards; i++) {
			int random = rng.nextInt(numberOfCards);
			Card storage = this.cards[random];
			this.cards[random] = this.cards[i];
			this.cards[i] = storage;
		}
	}
	
}